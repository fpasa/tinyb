# Routes Spec

Data in tinyb is organized in collections. Collections are containers
of documents and do not need to be created explicitly, and will be created
on the fly when needed. Documents are valid JSON objects that represent
some data.

The routes available to talk to the database are the following:

# Routes

## Queries

> GET /:collection

Return objects in the collection named `:collection`. Supports filter params.

> DELETE /:collection

Delete objects in the collection `:collection`. Supports filter params.

> PUT /:collection

Update objects in the collection `:collection`. Supports filter params.

The body of the request contains the JSON representing the replacement object.

> PATCH /:collection

Patch objects in the collection `:collection`. Supports filter params.

The body of the request contains the JSON representing the replacement or additional fields.
Existing fields are kept.

## Objects

> POST /:collection

Create a new object identified in `:collection` and return the created key.

The body of the request contains the JSON representing the object to create.

> GET /:collection/:key

Return object identified by `:key` in `:collection`.

> DELETE /:collection/:key

Delete object identified by `:key` in `:collection`.

> PUT /:collection/:key

Replace object identified by `:key` in `:collection`.

The body of the request contains the JSON representing the replacement object.

> PATCH /:collection/:key

Patch object identified by `:key` in `:collection`.

The body of the request contains the JSON representing the replacement or additional fields. 
Existing fields are kept.

## Secondary Indices

> GET /:collection/indices

Get list of existing secondary indices for the collection

> PUT /:collection/indices/:fields

Create a secondary index on fields `:fields`.

# Filter Parameters

WIP: not sure what to do with streaming

- `limit` (integer, default: 100) - Limit the number of records to return.
- `offset` (integer, default: 0) - Records to skip at the beginning of the collection.
- `order_by` (string) - If set, order records by the given field. Multiple fields can
                        be specified by passing a comma separated list.
- `where` (string) - Filter fields by 


Additional fields returned by 