package tests

import (
	"fmt"
	"math/rand"
	"sort"
	"testing"
	"time"
)

func genArray(size int) []uint64 {
	var array []uint64
	for i := 0; i < size; i++ {
		array = append(array, rand.Uint64())
	}
	return array
}

func TestSortSpeed(t *testing.T) {
	sizes := []int{100, 1000, 10_000, 100_000, 1_000_000}
	for _, size := range sizes {
		array := genArray(size)
		start := time.Now()
		sort.Slice(
			array,
			func(i, j int) bool { return array[i] < array[j] },
		)
		elapsed := time.Since(start)
		fmt.Printf("Size: %v - Duration: %v\n", size, elapsed)
	}
}
