package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"
)

var Object = []byte("{\"test\": \"body\"}")
var UpdatedObject = []byte("{\"test\": \"body2\", \"new_field\": 123}")

type KeysResponse struct {
	Keys []uint64 `json:"keys"`
}

func readKeysResponse(r *http.Response) KeysResponse {
	content, err := io.ReadAll(r.Body)
	handleError(err)

	var keys KeysResponse
	err = json.Unmarshal(content, &keys)
	handleError(err)

	return keys
}

// TODO: This test will not give right results
//       until database creation is implemented
func TestGetObject_DatabaseDoesNotExist(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	_, err := collection("data", "coll").CreateObject(Object)
	handleError(err)

	resp, err := http.Get("http://127.0.0.1:55443/database/DOES_NOT_EXIST/collection/coll/123")
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusNotFound)
	printBody(resp)
}

// TODO: This test will not give right results
//       until collection creation is implemented
func TestGetObject_CollectionDoesNotExist(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	_, err := collection("data", "coll").CreateObject(Object)
	handleError(err)

	resp, err := http.Get("http://127.0.0.1:55443/database/data/collection/DOES_NOT_EXIST/123")
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusNotFound)
	printBody(resp)
}

func TestGetObject_BadKey(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	resp, err := http.Get("http://127.0.0.1:55443/database/data/collection/coll/abcd")
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusBadRequest)
	printBody(resp)
}

func TestGetObject_ObjectDoesNotExist(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	resp, err := http.Get("http://127.0.0.1:55443/database/data/collection/coll/123")
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusNotFound)
	printBody(resp)
}

func TestGetObject(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	key, err := collection("data", "coll").CreateObject(Object)
	handleError(err)

	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:55443/database/data/collection/coll/%v", key))
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusOK)

	content, err := io.ReadAll(resp.Body)
	handleError(err)
	assertEqual(content, Object)
	resp.Body.Close()
}

func TestCreateObject(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	resp, err := http.Post(
		"http://127.0.0.1:55443/database/data/collection/coll",
		"application/json",
		bytes.NewBuffer(Object),
	)
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusCreated)

	keys := readKeysResponse(resp)
	assertEqual(len(keys.Keys), 1)
	resp.Body.Close()

	object, exists, err := collection("data", "coll").GetObject(keys.Keys[0])
	handleError(err)
	assertEqual(exists, true)
	assertEqual(object, Object)
}

func TestDeleteObject(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	key, err := collection("data", "coll").CreateObject(Object)
	handleError(err)

	req, err := http.NewRequest(
		"DELETE",
		fmt.Sprintf("http://127.0.0.1:55443/database/data/collection/coll/%v", key),
		nil,
	)
	handleError(err)

	resp, err := http.DefaultClient.Do(req)
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusOK)

	keys := readKeysResponse(resp)
	assertEqual(len(keys.Keys), 1)
	assertEqual(keys.Keys[0], key)
	resp.Body.Close()

	_, exists, err := collection("data", "coll").GetObject(keys.Keys[0])
	handleError(err)
	assertEqual(exists, false)
}

func TestUpdateObject(t *testing.T) {
	startDatabase()
	defer cleanUpDatabase()

	key, err := collection("data", "coll").CreateObject(Object)
	handleError(err)

	req, err := http.NewRequest(
		"PUT",
		fmt.Sprintf("http://127.0.0.1:55443/database/data/collection/coll/%v", key),
		bytes.NewBuffer(UpdatedObject),
	)
	handleError(err)

	resp, err := http.DefaultClient.Do(req)
	handleError(err)
	assertEqual(resp.StatusCode, http.StatusOK)

	keys := readKeysResponse(resp)
	assertEqual(len(keys.Keys), 1)
	assertEqual(keys.Keys[0], key)
	resp.Body.Close()

	object, exists, err := collection("data", "coll").GetObject(keys.Keys[0])
	handleError(err)
	assertEqual(exists, true)
	assertEqual(object, UpdatedObject)
}
