package tests

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"tinyb/tinyb"
)

var Options *tinyb.Options
var Server *http.Server
var WaitGroup *sync.WaitGroup

func handleError(err error) {
	if err != nil {
		log.Panicln(err)
	}
}

func assertEqual(actual interface{}, expected interface{}) {
	switch actual.(type) {
	case []byte:
		if !bytes.Equal(actual.([]byte), expected.([]byte)) {
			log.Fatalf(
				"Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n",
				string(actual.([]byte)),
				string(expected.([]byte)),
			)
		}

	case error:
		if !errors.Is(actual.(error), expected.(error)) {
			log.Fatalf(
				"Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n",
				string(actual.([]byte)),
				string(expected.([]byte)),
			)
		}

	default:
		if actual != expected {
			log.Panicf("Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n", actual, expected)
		}
	}
}

func startDatabase() {
	dataPath, err := os.MkdirTemp("", "tinyb-tests-*")
	handleError(err)

	Options = tinyb.DefaultOptions()
	Options.DataPath = dataPath

	Server, WaitGroup = tinyb.StartServer(Options)

	// Give some time for server goroutine to start-up
	time.Sleep(1 * time.Millisecond)
}

func cleanUpDatabase() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := Server.Shutdown(ctx)
	handleError(err)

	WaitGroup.Wait()

	err = os.RemoveAll(Options.DataPath)
	handleError(err)
}

func database(databaseName string) *tinyb.Database {
	database, err := tinyb.NewState(Options).GetDatabase(databaseName, Options)
	handleError(err)
	return database
}

func collection(databaseName string, collectionName string) *tinyb.Collection {
	collection, err := database(databaseName).GetCollection(collectionName)
	handleError(err)
	return collection
}

func printBody(r *http.Response) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	handleError(err)
	fmt.Println("Response body:\n", string(body))
}
