# tinyb

tinyb is an experimental database focused on simplicity, minimalism and
performance. tinyb is a document database, that uses a log-oriented storage
engine and exposes a REST API that facilitates writing drivers 
for different languages. Using the database without a driver is also 
rather simple.

tinyb is written in Go.

## Current State

- Right now tinyb only works as a key-value store.

## Current Goals

- Keeping the database small and simple, while offering 
  a rather complete feature set.
- Streaming data from database (e.g. act as an event stream).
- Very few dependencies.

## Current Non-Goals

These might become goals in the future.

- Scalability to multiple nodes.
- Joins.
- Language clients.
- GUI clients.
