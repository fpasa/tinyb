package main

import (
	"flag"
	"tinyb/tinyb"
)

func main() {
	options := parseArgs()
	_, waitGroup := tinyb.StartServer(options)
	waitGroup.Wait()
}

func parseArgs() *tinyb.Options {
	options := tinyb.DefaultOptions()

	flag.StringVar(&options.DataPath, "data-path", "./tinyb_data", "Path where the database data will be stored.")
	flag.StringVar(&options.Port, "port", "55443", "Port to listen to (e.g. 55443).")
	flag.StringVar(&options.Host, "host", "0.0.0.0", "Host to listen to (e.g. 0.0.0.0).")

	flag.Parse()
	return options
}
