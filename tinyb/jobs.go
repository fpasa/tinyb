package tinyb

// This file contains a background job goroutine.
//
// The goal of the background jobs is that some internal database
// processes such as flushing segments, compactation and index creation
// can take a long time, so they can be executed asynchronously.
//
// However, we still want to control the concurrency, to avoid putting
// too much load on the machine. For instance, log compactation is a I/O
// heavy operation and might slow down the reads too much if multiple
// compactation jobs are executed at the same time.
//
// On the other hand, flushing and index creation require sorting large
// amounts of data and are thus computationally intensive, so we might
// only allow a few to happen at the same time.
//
// Right now this implementation only handles jobs sequentially in a single
// goroutine.

import (
	"log"
)

// TODO: Check what the correct size of the channel should be
//       or make it configurable
var jobQueue = make(chan Message, 1000)

func sendJob(job Message) {
	jobQueue <- job
}

func runJobs(database *Database) {
	go preloadCollections(database)

	// Listen for background processes
	handleJob(database.options)
}

// Runs "lightweight" background jobs such as
// flushing memory segments to disk.
func handleJob(options *Options) {
loop:
	for {
		// TODO: compact message queue to avoid executing the same command
		//       many times after each other. Alternatively, debounce
		//       messages when sent.
		message := <-jobQueue
		switch job := message.(type) {
		case FlushMemSegment:
			flushMemSegment(job.memSegment)
		case ExitMessage:
			break loop
		default:
			log.Panicf("Hander for job %v not yet implemented\n", message)
		}
	}
}

func flushMemSegment(memSegment *MemSegment) {
	_, err := memSegment.Flush()
	if err != nil {
		log.Printf("Error flushing segment to disk: %v\n", err)
		return
	}
}
