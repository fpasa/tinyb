package tinyb

import (
	"encoding/binary"
	"io"
	"os"
)

const (
	SearchBufferSize = int64(1024)
)

// A Segment is a read-only, sorted log of data.
//
// Segments are very similar to MemSegments
// and are stored using the same binary log format.
// The difference is that key-value pairs are stored
// sorted by key.
//
// This entails that key-value paris do not appear in
// insertion order as in the case of logs.
// The advantage of sorting is that a Segment,
// instead of keeping the full hash table of the data
// in memory like a MemSegment does, it can keep a
// sparse index,  e.g. a list of key-offset pairs,
// one key every few kilobytes of data.
//
// When querying, if the key is not found in the list,
// we know that it must lie between the offset of the
// closest keys, and can therefore load and search only
// this part of the data.
//
// This allows a collection to store more data than the
// available memory, but still quickly retrieve data when
// necessary.
type Segment struct {
	segmentPath string
	index       []KeyOffsetPair
	file        *os.File
}

type KeyOffsetPair struct {
	key    uint64
	offset uint64
}

func SegmentFromFile(segmentPath string, blockSize uint64) (*Segment, error) {
	file, err := os.Open(segmentPath)
	if err != nil {
		return nil, err
	}

	var key uint64
	var index []KeyOffsetPair
	offset := int64(0)
	for {
		err := binary.Read(file, binary.LittleEndian, &key)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		index = append(index, KeyOffsetPair{key, uint64(offset)})

		// the -8 is there to account for the key size
		offset, err = file.Seek(int64(blockSize)-8, 1)
		if err != nil {
			return nil, err
		}

		// search for next record separator
		offset, err = bufferedForwardSearch(file, offset)
		if err != nil {
			return nil, err
		}
	}

	// Ensure the last key is also in the index, because
	// this allows to quickly exclude keys that lie outside
	// the segment key range.
	//
	// The Seek(-1) starts the search from the second last byte,
	// because the last byte is a record separator.
	offset, err = file.Seek(-1, 2)
	if err != nil {
		return nil, err
	}
	offset, err = bufferedBackwardSearch(file, offset)
	if err != nil {
		return nil, err
	}

	err = binary.Read(file, binary.LittleEndian, &key)
	if err != nil {
		return nil, err
	}

	index = append(index, KeyOffsetPair{key, uint64(offset)})

	return &Segment{
		segmentPath: segmentPath,
		index:       index,
		file:        file,
	}, nil
}

func (s *Segment) LastPair() KeyOffsetPair {
	return s.index[len(s.index)-1]
}

func (s *Segment) Get(key uint64) ([]byte, bool, error) {
	// Check if the key lies outside the bounds of the segment
	if key < s.index[0].key {
		return nil, false, nil
	} else if s.LastPair().key < key {
		return nil, false, nil
	}

	// TODO: use binary search to speed up lookup
	var blockStart, blockEnd uint64
	for i, pair := range s.index {
		if pair.key < key {
			blockStart = pair.offset
			blockEnd = s.index[i+1].offset
		}
	}

	// TODO: see if this search can be optimized
	offset, err := s.file.Seek(int64(blockStart), 0)
	if err != nil {
		return nil, false, err
	}
	for {
		offsetKey, value, err := ReadRecord(s.file)
		if err != nil {
			return nil, false, err
		}
		if key == offsetKey {
			return value, true, nil
		}
		// 13 is the record overhead in bytes (8 for key,
		// 4 for length, 1 for record separator)
		offset += int64(len(value)) + 13

		if uint64(offset) > blockEnd {
			break
		}
	}

	return nil, false, nil
}

func bufferedForwardSearch(file *os.File, offset int64) (int64, error) {
	buffer := make([]byte, SearchBufferSize)
	for {
		n, err := file.Read(buffer)
		if err == io.EOF {
			return 0, nil
		} else if err != nil {
			return 0, err
		}

		for i := 0; i < n; i++ {
			if buffer[i] == RecordSeparator {
				// +1 to seek the position after the RecordSeparator
				return file.Seek(int64(i-n+1), 1)
			}
		}

		offset, err = file.Seek(SearchBufferSize, 1)
		if err != nil {
			return 0, err
		}
	}
}

func bufferedBackwardSearch(file *os.File, offset int64) (int64, error) {
	newOffset := offset - SearchBufferSize
	if newOffset < 0 {
		newOffset = 0
	}
	offset, err := file.Seek(newOffset, 0)
	if err != nil {
		return 0, err
	}

	buffer := make([]byte, SearchBufferSize)
	for {
		n, err := file.Read(buffer)
		if err == io.EOF {
			return 0, nil
		} else if err != nil {
			return 0, err
		}

		// In this case the file is smaller than the SearchBufferSize,
		// and therefore we ended up in the final position again.
		// We exclude the last character by cheating on the size of
		// the buffer.
		if int64(n) < SearchBufferSize {
			n -= 1
		}

		for i := n - 1; i < n; i-- {
			if buffer[i] == RecordSeparator {
				// +1 to seek the position after the RecordSeparator
				offset, err = file.Seek(int64(i-n+1), 1)
				if err != nil {
					return 0, err
				}
				return offset, nil
			}
		}

		// The read operation moves us ahead again,
		// so we need to compensate by multiplying by 2
		newOffset := offset - 2*SearchBufferSize
		if newOffset < 0 {
			newOffset = 0
		}
		offset, err = file.Seek(newOffset, 0)
		if err != nil {
			return 0, err
		}
	}
}
