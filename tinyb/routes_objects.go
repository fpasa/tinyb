package tinyb

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

func getObjectById(
	w http.ResponseWriter,
	_ *http.Request,
	params httprouter.Params,
	_ *Database,
	collection *Collection,
) {
	key, err := strconv.ParseUint(params.ByName("objectKey"), 10, 64)
	if err != nil {
		sendError(
			w, err, http.StatusBadRequest,
			fmt.Sprintf("Invalid object key: '%v'", params.ByName("objectKey")),
		)
		return
	}

	value, exists, err := collection.GetObject(key)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Could not fetch object",
		)
		return
	}

	if exists {
		_, err = w.Write(value)
	} else {
		w.WriteHeader(http.StatusNotFound)
		errorMessage := fmt.Sprintf(
			"{\"error\": \"Collection '%v' in database '%v' does not contain key '%v'\"}",
			params.ByName("collectionName"),
			params.ByName("databaseName"),
			key,
		)
		_, err = w.Write([]byte(errorMessage))
	}

	sendError(
		w, err, http.StatusInternalServerError,
		"Cannot write response body",
	)
}

func createObject(
	w http.ResponseWriter,
	r *http.Request,
	_ httprouter.Params,
	_ *Database,
	collection *Collection,
) {
	value, err := readBody(r)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Cannot read request body",
		)
		return
	}

	key, err := collection.CreateObject(value)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Could not create object",
		)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write([]byte(fmt.Sprintf("{\"keys\": [%v]}", key)))
	sendError(
		w, err, http.StatusInternalServerError,
		"Cannot write response body",
	)
}

func deleteObject(
	w http.ResponseWriter,
	_ *http.Request,
	params httprouter.Params,
	_ *Database,
	collection *Collection,
) {
	key, err := strconv.ParseUint(params.ByName("objectKey"), 10, 64)
	if err != nil {
		sendError(
			w, err, http.StatusBadRequest,
			fmt.Sprintf("Invalid object key: '%v'", params.ByName("objectKey")),
		)
		return
	}

	err = collection.DeleteObject(key)
	if err == NotFoundError {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Could not delete object",
		)
		return
	}

	_, err = w.Write([]byte(fmt.Sprintf("{\"keys\": [%v]}", key)))
	sendError(
		w, err, http.StatusInternalServerError,
		"Cannot write response body",
	)
}

func createOrUpdateObject(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
	_ *Database,
	collection *Collection,
) {
	key, err := strconv.ParseUint(params.ByName("objectKey"), 10, 64)
	if err != nil {
		sendError(
			w, err, http.StatusBadRequest,
			fmt.Sprintf("Invalid object key: '%v'", params.ByName("objectKey")),
		)
		return
	}

	value, err := readBody(r)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Cannot read request body",
		)
		return
	}

	err = collection.SetObject(key, value)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Could not set object",
		)
		return
	}

	_, err = w.Write([]byte(fmt.Sprintf("{\"keys\": [%v]}", key)))
	sendError(
		w, err, http.StatusInternalServerError,
		"Cannot write response body",
	)
}
