package tinyb

import (
	"path/filepath"
	"testing"
	"time"
)

func database(path string) *Database {
	options := DefaultOptions()
	options.DataPath = path
	return NewDatabase(options)
}

func TestDatabase_NewDatabase(t *testing.T) {
	db := database(t.TempDir())
	assertEqual(len(db.collections), 0)
}

func TestDatabase_loadCollection(t *testing.T) {
	db := database(t.TempDir())
	err := db.loadCollection("test_collection", db.options)
	handleError(err)

	assertEqual(len(db.collections), 1)
	_, exists := db.collections["test_collection"]
	assertEqual(exists, true)

	// loading the same correction another time,
	// should not trigger any loading
	err = db.loadCollection("test_collection", db.options)
	handleError(err)
	assertEqual(len(db.collections), 1)
}

// Test that loadCollection properly returns errors
func TestDatabase_loadCollection_error(t *testing.T) {
	// Trigger an error by using an invalid path composed of a null byte.
	// This triggers an error on all systems I know of
	db := database("\x00")
	err := db.loadCollection("test_collection", db.options)
	assertEqual(err.Error(), "mkdir \x00: invalid argument")
}

func TestDatabase_Close(t *testing.T) {
	db := database(t.TempDir())
	db.Close()

	// Try to send a message, since the worker should be
	// stopped, the timeout should take effect
	select {
	case db.loadCollectionQueue <- struct{}{}:
		t.Fail()
	case <-time.After(10 * time.Millisecond):
	}
}

func TestDatabase_GetCollection(t *testing.T) {
	path := t.TempDir()

	db := database(path)
	collection, err := db.GetCollection("test_collection")
	handleError(err)
	assertEqual(
		collection.collectionPath,
		filepath.Join(path, "test_collection"),
	)

	// The second time should return the cached collection
	collection2, err := db.GetCollection("test_collection")
	handleError(err)
	assertEqual(collection2, collection)
}
