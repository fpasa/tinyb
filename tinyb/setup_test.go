package tinyb

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
)

const (
	LogFilePath     = "test_data/log-0"
	SegmentFilePath = "test_data/segment-0"
	// 4 MB log file
	//
	// The keys start with 51_453 and end at 100_010,
	// for a total of 48_557 objects.
	// Objects are generated with GetObject.
	LogLargeFilePath = "test_data/log-large"
	// 4 MB segment file
	//
	// The keys start with 10 and end with 51_452.
	// Objects are generated with GetObject.
	SegmentLargeFilePath = "test_data/segment-large"
)

var Object = []byte("{\"name\": \"John\", \"age\": 27, \"occupation\": \"Painter\"}")

func handleError(err error) {
	if err != nil {
		log.Panicln(err)
	}
}

func assertEqual(actual interface{}, expected interface{}) {
	switch actual.(type) {
	case []byte:
		if !bytes.Equal(actual.([]byte), expected.([]byte)) {
			log.Fatalf(
				"Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n",
				string(actual.([]byte)),
				string(expected.([]byte)),
			)
		}

	case error:
		if !errors.Is(actual.(error), expected.(error)) {
			log.Fatalf(
				"Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n",
				string(actual.([]byte)),
				string(expected.([]byte)),
			)
		}

	default:
		if actual != expected {
			log.Panicf("Assertion not satisfied:\n\nActual:   %v\nExpected: %v\n\n", actual, expected)
		}
	}
}

func assertFileExists(path string) {
	_, err := os.Stat(path)
	assertEqual(err, nil)
}

func assertFileDoesNotExists(path string) {
	_, err := os.Stat(path)
	assertEqual(err, os.ErrNotExist)
}

func GetObject(num int) []byte {
	return []byte(fmt.Sprintf(
		"{\"id\": %v, \"name\": \"John\", \"age\": 27, \"occupation\": \"Painter\"}",
		num,
	))
}
