package tinyb

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
)

type MemSegment struct {
	collectionPath string
	number         uint64
	records        map[uint64][]byte
	file           *os.File
}

func NewMemSegment(collectionPath string, number uint64) (*MemSegment, error) {
	logFilename := fmt.Sprintf("log-%v", number)
	logPath := filepath.Join(collectionPath, logFilename)

	file, err := os.OpenFile(
		logPath,
		os.O_WRONLY|os.O_CREATE|os.O_APPEND,
		0o600,
	)
	if err != nil {
		return nil, err
	}

	return &MemSegment{
		collectionPath: collectionPath,
		number:         number,
		records:        make(map[uint64][]byte),
		file:           file,
	}, nil
}

func MemSegmentFromLog(logPath string, number uint64) (*MemSegment, error) {
	collectionPath := filepath.Dir(logPath)

	file, err := os.OpenFile(
		logPath,
		os.O_RDWR|os.O_APPEND,
		0o600,
	)
	if err != nil {
		return nil, err
	}

	records := make(map[uint64][]byte)
	for {
		key, value, err := ReadRecord(file)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		// The record was deleted
		if len(value) == 0 {
			delete(records, key)
			continue
		}

		records[key] = value
	}

	return &MemSegment{
		collectionPath: collectionPath,
		number:         number,
		records:        records,
		file:           file,
	}, nil
}

func (s *MemSegment) Get(key uint64) ([]byte, bool) {
	value, exists := s.records[key]

	if len(value) == 0 {
		return nil, false
	}

	return value, exists
}

func (s *MemSegment) Set(key uint64, value []byte) error {
	err := WriteRecord(s.file, key, value)
	if err != nil {
		return err
	}
	s.records[key] = value
	return nil
}

func (s *MemSegment) Delete(key uint64) error {
	if _, exists := s.records[key]; !exists {
		return NotFoundError
	}
	return s.Set(key, []byte(""))
}

func (s *MemSegment) Size() (uint64, error) {
	stats, err := s.file.Stat()
	if err != nil {
		return 0, err
	}
	return uint64(stats.Size()), nil
}

// Flush memory segment to disk, effectively
// converting it to a normal segment.
//
// This is a I/O and computationally heavy operation.
//
// First write to .part file. Once the write is
// complete, rename .part and delete log file.
// This ensures that in the event of a crash
// at any time we can recover the state.
func (s *MemSegment) Flush() (string, error) {
	// Close to ensure that no more writes are going
	// to be performed.
	err := s.file.Close()
	if err != nil {
		return "", err
	}

	logPath := s.file.Name()
	segmentPath := filepath.Join(
		s.collectionPath,
		fmt.Sprintf("segment-%v", s.number),
	)
	segmentPartPath := filepath.Join(
		s.collectionPath,
		fmt.Sprintf("segment-%v.part", s.number),
	)

	file, err := os.OpenFile(
		segmentPartPath,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
		0o600,
	)
	if err != nil {
		return "", err
	}

	var keys []uint64
	for key, value := range s.records {
		// Exclude deleted keys. This also improves
		// performances if there are many deleted keys.
		if len(value) == 0 {
			continue
		}
		keys = append(keys, key)
	}

	sort.Slice(
		keys,
		func(i, j int) bool { return keys[i] < keys[j] },
	)

	var buffer bytes.Buffer
	for _, key := range keys {
		err := WriteRecord(&buffer, key, s.records[key])
		if err != nil {
			return "", err
		}
	}

	_, err = file.Write(buffer.Bytes())
	if err != nil {
		return "", err
	}

	err = os.Rename(segmentPartPath, segmentPath)
	if err != nil {
		return "", err
	}

	return segmentPath, os.Remove(logPath)
}
