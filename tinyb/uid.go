package tinyb

import (
	"math/rand"
)

func newId() uint64 {
	return rand.Uint64()
}
