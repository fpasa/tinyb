package tinyb

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

const (
	RecordSeparator = byte(0x1E)
	_GibibyteSize   = 1_073_741_824.
	MaxRecordLength = 4_294_967_295 // 2**32 - 1
)

func WriteRecord(w io.Writer, key uint64, value []byte) error {
	err := binary.Write(w, binary.LittleEndian, key)
	if err != nil {
		return err
	}
	if len(value) > MaxRecordLength {
		return errors.New(fmt.Sprintf(
			"Length of record cannot exceed 4 GiB: %v GiB",
			float64(len(value))/_GibibyteSize,
		))
	}
	err = binary.Write(w, binary.LittleEndian, uint32(len(value)))
	if err != nil {
		return err
	}
	_, err = w.Write(value)
	if err != nil {
		return err
	}
	// TODO: Escape record separator
	err = binary.Write(w, binary.LittleEndian, RecordSeparator)
	return err
}

func ReadRecord(r io.Reader) (uint64, []byte, error) {
	var key uint64
	var length uint32
	err := binary.Read(r, binary.LittleEndian, &key)
	if err != nil {
		return key, nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &length)
	if err != nil {
		return key, nil, err
	}
	value := make([]byte, length)
	_, err = r.Read(value)
	if err != nil {
		return key, value, err
	}
	// TODO: Unescape record separator
	// Read record separator to consume reader
	// up to the start of the next record
	buf := []byte{0}
	_, err = r.Read(buf)
	return key, value, err
}
