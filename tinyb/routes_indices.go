package tinyb

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

func createOrUpdateIndex(
	w http.ResponseWriter,
	_ *http.Request,
	params httprouter.Params,
	_ *Database,
	collection *Collection,
) {
	key, err := strconv.ParseUint(params.ByName("objectKey"), 10, 64)
	if err != nil {
		sendError(
			w, err, http.StatusBadRequest,
			fmt.Sprintf("Invalid object key: '%v'", params.ByName("objectKey")),
		)
		return
	}

	value, exists, err := collection.GetObject(key)
	if err != nil {
		sendError(
			w, err, http.StatusInternalServerError,
			"Could not fetch object",
		)
		return
	}

	if exists {
		_, err = w.Write(value)
	} else {
		w.WriteHeader(http.StatusNotFound)
		errorMessage := fmt.Sprintf(
			"{\"error\": \"Collection '%v' in database '%v' does not contain key '%v'\"}",
			params.ByName("collectionName"),
			params.ByName("databaseName"),
			key,
		)
		_, err = w.Write([]byte(errorMessage))
	}

	sendError(
		w, err, http.StatusInternalServerError,
		"Cannot write response body",
	)
}
