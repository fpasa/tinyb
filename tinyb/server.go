package tinyb

import (
	"log"
	"net/http"
	"sync"
)

func StartServer(options *Options) (*http.Server, *sync.WaitGroup) {
	// Create database server object
	state := NewDatabase(options)

	// Create mux and server to manage routes
	router := defineRoutes(state)
	server := &http.Server{
		Addr:    options.Host + ":" + options.Port,
		Handler: router,
	}

	waitGroup := &sync.WaitGroup{}
	waitGroup.Add(1)

	go func() {
		log.Println("Starting tinyb database...")
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Panicln(err)
		}

		waitGroup.Done()
	}()

	// Start background jobs
	go runJobs(state)

	return server, waitGroup
}
