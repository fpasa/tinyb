package tinyb

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func collection(path string) *Collection {
	collection, err := loadCollection(path, DefaultOptions())
	handleError(err)
	return collection
}

func existingCollection(path string) *Collection {
	// Create a fake collection folder
	copyFile(LogLargeFilePath, filepath.Join(path, "log-1"))
	copyFile(SegmentLargeFilePath, filepath.Join(path, "segment-0"))
	return collection(path)
}

func indicesCollection(path string) *Collection {
	// Create a fake collection folder with indices
	err := os.MkdirAll(filepath.Join(path, "indices", "birth_year"), 0700)
	handleError(err)
	err = os.MkdirAll(filepath.Join(path, "indices", "first_name,second_name"), 0700)
	handleError(err)
	return existingCollection(path)
}

func copyFile(src string, dst string) {
	srcFile, err := os.Open(src)
	handleError(err)
	dstFile, err := os.Create(dst)
	handleError(err)

	_, err = io.Copy(dstFile, srcFile)
	handleError(err)

	err = srcFile.Close()
	handleError(err)
	err = dstFile.Close()
	handleError(err)
}

func TestCollection_LoadCollection_Empty(t *testing.T) {
	collectionPath := t.TempDir()
	coll := collection(collectionPath)
	assertEqual(coll.collectionPath, collectionPath)
	assertEqual(len(coll.mem.records), 0)
	assertEqual(len(coll.orphanedMemSegments), 0)
	assertEqual(len(coll.segments), 0)
}

func TestCollection_LoadCollection_Segments(t *testing.T) {
	collectionPath := t.TempDir()
	coll := existingCollection(collectionPath)
	assertEqual(coll.collectionPath, collectionPath)
	assertEqual(len(coll.mem.records), 48_557)
	assertEqual(len(coll.orphanedMemSegments), 0)
	assertEqual(len(coll.segments), 1)
	assertEqual(len(coll.segments[0].index), 486)
}

func TestCollection_LoadCollection_NoIndicesFolder(t *testing.T) {
	collectionPath := t.TempDir()
	coll := collection(collectionPath)
	assertEqual(coll.collectionPath, collectionPath)
	assertEqual(len(coll.indices), 0)
}

func TestCollection_LoadCollection_NoIndices(t *testing.T) {
	collectionPath := t.TempDir()
	err := os.MkdirAll(filepath.Join(collectionPath, "indices"), 0700)
	handleError(err)

	coll := collection(collectionPath)
	assertEqual(coll.collectionPath, collectionPath)
	assertEqual(len(coll.indices), 0)
}

func TestCollection_LoadCollection_Indices(t *testing.T) {
	collectionPath := t.TempDir()
	coll := indicesCollection(collectionPath)
	assertEqual(coll.collectionPath, collectionPath)
	assertEqual(len(coll.indices), 2)
}

func TestCollection_CreateObject(t *testing.T) {
	coll := collection(t.TempDir())

	assertEqual(len(coll.mem.records), 0)
	assertEqual(len(jobQueue), 0)

	_, err := coll.CreateObject([]byte("value"))
	handleError(err)
	assertEqual(len(coll.mem.records), 1)
	assertEqual(len(jobQueue), 1)
}

func TestCollection_GetObject(t *testing.T) {
	coll := collection(t.TempDir())

	value, exists, err := coll.GetObject(1234)
	handleError(err)
	assertEqual(exists, false)

	key, err := coll.CreateObject(Object)
	handleError(err)

	value, exists, err = coll.GetObject(key)
	handleError(err)
	assertEqual(exists, true)
	assertEqual(value, Object)
}

// Test and benchmark speed of different operations
// on a realistic scenario with thousands of objects:
//
// - Inserting 100k records.
// - Querying 100k records.
//
// This test also tries to flush a segment while writes
// are happening, thus simulating what might happen in
// a realistic load scenario.
func TestCollection_GetObject_Performance(t *testing.T) {
	// Start job handler goroutine. This is responsible
	// for reacting to collection changes and flushing
	// MemSegments to disk once they grow over the threshold.
	//
	// Basically, it simulates how the database work, without
	// starting the entire database machine.
	go handleJob(DefaultOptions())

	coll := existingCollection(t.TempDir())
	assertEqual(len(coll.segments), 1)

	start := time.Now()
	for i := 0; i < 100_000; i++ {
		_, err := coll.CreateObject(Object)
		handleError(err)
	}
	elapsed := time.Since(start)

	assertEqual(len(coll.segments), 3)
	size, err := coll.mem.Size()
	handleError(err)
	fmt.Printf("Mem segment size: %v kB\n", size/1000)

	for i := 0; i < 3; i++ {
		stats, err := coll.segments[i].file.Stat()
		handleError(err)
		fmt.Printf("Segment %v size: %v kB\n", i, stats.Size()/1000)
	}

	fmt.Printf("Insert: %v\n", elapsed)

	start = time.Now()
	// Note that this is a worst case scenario, as all
	// keys belong to the oldest segment.
	// Sampling with 101 executes about 1000 read operations
	for i := 10; i <= 100_010; i += 101 {
		_, exists, err := coll.GetObject(uint64(i))
		handleError(err)
		assertEqual(exists, true)
	}
	elapsed = time.Since(start)

	fmt.Printf("Query by key: %v\n", elapsed)

	// Stop job handler goroutine
	sendJob(ExitMessage{})
}
