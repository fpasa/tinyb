package tinyb

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io"
	"log"
	"net/http"
	"runtime/debug"
)

func sendError(
	w http.ResponseWriter,
	err error,
	statusCode int,
	message string,
) {
	if err == nil {
		return
	}

	// TODO: do this only in debug mode
	log.Println(string(debug.Stack()))
	log.Printf("%v: %v", message, err)

	w.WriteHeader(statusCode)
	_, err = w.Write([]byte(fmt.Sprintf(
		"{\"error\": \"%v\"}",
		message,
	)))
	if err != nil {
		log.Println(err)
	}
}

func readBody(r *http.Request) ([]byte, error) {
	buf, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	err = r.Body.Close()
	if err != nil {
		return nil, err
	}

	return buf, nil
}

type CollectionHandler func(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
	database *Database,
	collection *Collection,
)

func collectionRoute(database *Database, handler CollectionHandler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		collectionName := params.ByName("collectionName")

		collection, err := database.GetCollection(collectionName)
		if err != nil {
			sendError(
				w, err, http.StatusNotFound,
				fmt.Sprintf("Collection '%v' not found", collectionName),
			)
			return
		}

		handler(w, r, params, database, collection)
	}
}

func defineRoutes(database *Database) *httprouter.Router {
	router := httprouter.New()

	router.GET(
		"/collections/:collectionName/docs/:objectKey",
		collectionRoute(database, getObjectById),
	)
	router.POST(
		"/collections/:collectionName",
		collectionRoute(database, createObject),
	)
	router.DELETE(
		"/collections/:collectionName/docs/:objectKey",
		collectionRoute(database, deleteObject),
	)
	router.PUT(
		"/collections/:collectionName/docs/:objectKey",
		collectionRoute(database, createOrUpdateObject),
	)

	router.PUT(
		"/collections/:collectionName/indices/:fields",
		collectionRoute(database, createOrUpdateIndex),
	)

	return router
}
