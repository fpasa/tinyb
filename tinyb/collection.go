package tinyb

import (
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

type Collection struct {
	// Path of the folder storing the collection data on disk.
	collectionPath string
	// Pointer to options for the collection
	options *Options

	// DATA
	// Current memory segment used for writing.
	mem *MemSegment
	// List of previous memory segments.
	//
	// When the configured flush size is exceeded,
	// the active memory segment is replaced by a fresh segment
	// while the old segment is flushed to disk
	//
	// The list should usually be empty, but can be filled
	// during flushing, when the system is under high load
	// and many flushes need to be queued, or in case of a
	// crash while the flushing was in progress.
	orphanedMemSegments []*MemSegment
	// TODO: add docs
	segments []*Segment

	// INDICES
	// Table of the secondary indices in the collection
	//
	// The indices are loaded when the collection is loaded,
	// so the map is guaranteed to be complete.
	indices map[string]*Collection
}

func loadCollection(collectionPath string, options *Options) (*Collection, error) {
	err := createCollectionFolder(collectionPath)
	if err != nil {
		return nil, err
	}

	collectionDir, err := os.OpenFile(collectionPath, 0, 0o600)
	if err != nil {
		return nil, err
	}
	collectionEntries, err := collectionDir.ReadDir(-1)
	if err != nil && err != io.EOF {
		return nil, err
	}

	logFiles := filterLogFiles(collectionPath, collectionEntries)
	segmentFiles := filterSegmentFiles(collectionPath, collectionEntries)

	memSegment, err := getMemSegment(collectionPath, logFiles)
	if err != nil {
		return nil, err
	}

	// Load other mem segment as orphaned segments
	// TODO: check if a corresponding segment was already
	//       flushed and delete log in that case
	orphanedMemSegments, err := loadOrphanedMemSegments(logFiles)
	if err != nil {
		return nil, err
	}

	segments, err := loadSegments(segmentFiles, options)
	if err != nil {
		return nil, err
	}

	indicesPath := filepath.Join(collectionPath, "indices")
	indices, err := loadIndices(indicesPath, options)
	if err != nil {
		return nil, err
	}

	return &Collection{
		collectionPath:      collectionPath,
		options:             options,
		mem:                 memSegment,
		orphanedMemSegments: orphanedMemSegments,
		segments:            segments,
		indices:             indices,
	}, err
}

func createCollectionFolder(collectionPath string) error {
	err := os.MkdirAll(collectionPath, 0o700)
	if err != nil {
		return err
	}

	indicesPath := filepath.Join(collectionPath, "indices")
	err = os.MkdirAll(indicesPath, 0o700)
	if err != nil {
		return err
	}

	return nil
}

func filterLogFiles(collectionPath string, entries []fs.DirEntry) []string {
	var logFiles []string
	for _, entry := range entries {
		prefix := strings.SplitN(entry.Name(), "-", 2)[0]
		if prefix == "log" {
			logPath := filepath.Join(collectionPath, entry.Name())
			logFiles = append(logFiles, logPath)
		}
	}

	sort.Strings(logFiles)

	return logFiles
}

func filterSegmentFiles(collectionPath string, entries []fs.DirEntry) []string {
	var segmentFiles []string
	for _, entry := range entries {
		prefix := strings.SplitN(entry.Name(), "-", 2)[0]
		if prefix == "segment" {
			logPath := filepath.Join(collectionPath, entry.Name())
			segmentFiles = append(segmentFiles, logPath)
		}
	}

	sort.Strings(segmentFiles)

	return segmentFiles
}

func getMemSegment(collectionPath string, logs []string) (*MemSegment, error) {
	if len(logs) == 0 {
		return NewMemSegment(collectionPath, 0)
	}

	logPath := logs[len(logs)-1]
	return loadMemSegment(logPath)
}

func loadOrphanedMemSegments(logFiles []string) ([]*MemSegment, error) {
	var orphanedMemSegments []*MemSegment
	for i := 0; i < len(logFiles)-1; i++ {
		logPath := logFiles[i]
		orphanedMemSegment, err := loadMemSegment(logPath)
		if err != nil {
			return nil, err
		}
		orphanedMemSegments = append(orphanedMemSegments, orphanedMemSegment)
	}
	return orphanedMemSegments, nil
}

func loadMemSegment(logPath string) (*MemSegment, error) {
	filename := filepath.Base(logPath)
	number, err := strconv.ParseUint(filename[4:], 10, 64)
	if err != nil {
		return nil, err
	}
	return MemSegmentFromLog(logPath, number)
}

func loadSegments(segmentFiles []string, options *Options) ([]*Segment, error) {
	var segments []*Segment
	for i := 0; i < len(segmentFiles); i++ {
		segmentPath := segmentFiles[i]
		segment, err := SegmentFromFile(segmentPath, options.IndexBlockSize)
		if err != nil {
			return nil, err
		}
		segments = append(segments, segment)
	}
	return segments, nil
}

func loadIndices(indicesPath string, options *Options) (map[string]*Collection, error) {
	dir, err := os.OpenFile(indicesPath, 0, 0o600)
	if err != nil {
		return nil, err
	}
	entries, err := dir.ReadDir(-1)
	if err != nil && err != io.EOF {
		return nil, err
	}

	indices := make(map[string]*Collection)
	for _, entry := range entries {
		indexCollectionPath := filepath.Join(indicesPath, entry.Name())
		collection, err := LoadCollection(indexCollectionPath, options)
		if err != nil {
			return nil, err
		}
		indices[entry.Name()] = collection
	}
	return indices, nil
}

func (c *Collection) CreateObject(value []byte) (uint64, error) {
	key := newId()

	err := c.mem.Set(key, value)
	if err != nil {
		return 0, err
	}

	sendJob(CheckNewMemSegmentNeeded{c})

	return key, err
}

// GetObject queries the collection segments to find the value.
//
// The look-up happens in the following order, in order of age:
//
//   1. Search the current memory segment.
//   2. Search orphaned memory segments in reverse order.
//      The newest orphaned segment has the highest log number.
//   3. Search in each segment in reverse order.
//      The newest segment has the higher file number.
func (c *Collection) GetObject(key uint64) ([]byte, bool, error) {
	value, exists := c.mem.Get(key)
	if exists {
		return value, exists, nil
	}

	// The key was deleted
	if len(value) == 0 {
		exists = false
	}

	for i := len(c.orphanedMemSegments) - 1; i >= 0; i-- {
		orphanedMemSegment := c.orphanedMemSegments[i]
		value, exists := orphanedMemSegment.Get(key)
		if exists {
			if len(value) == 0 {
				// The last entry is a deletion,
				// therefore the key does not exist
				exists = false
			}
			return value, exists, nil
		}
	}

	for i := len(c.segments) - 1; i >= 0; i-- {
		var err error

		segment := c.segments[i]
		value, exists, err = segment.Get(key)
		if err != nil {
			return nil, false, err
		}
		if exists {
			return value, exists, nil
		}
	}

	return nil, false, nil
}

func (c *Collection) DeleteObject(key uint64) error {
	err := c.mem.Delete(key)
	if err != nil {
		return err
	}

	sendJob(CheckNewMemSegmentNeeded{c})

	return nil
}

func (c *Collection) SetObject(key uint64, value []byte) error {
	err := c.mem.Set(key, value)
	if err != nil {
		return err
	}

	sendJob(CheckNewMemSegmentNeeded{c})

	return nil
}
