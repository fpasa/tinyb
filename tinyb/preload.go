package tinyb

import (
	"io"
	"log"
	"os"
)

// Preloading means loading all existing collections
// into the database object.
//
// Preloading is triggered at database startup and
// makes the database faster at the cost of increased
// memory consumption. Preloading is also necessary
// to recover from crashes as it re-triggers interrupted flushing.
//
// TODO: consider if preloading is a good idea or everything can
//       be done lazily to save memory. Loaded collections could
//       also be unloaded after a period of time to reduce memory
//       usage. This could be a setting.

func preloadCollections(database *Database) {
	dir, err := os.OpenFile(database.options.DataPath, 0, 0o600)
	if err != nil {
		log.Printf("Error reading collection folders: %v\n", err)
		return
	}
	entries, err := dir.ReadDir(-1)
	if err != nil && err != io.EOF {
		log.Printf("Error reading collection folders: %v\n", err)
		return
	}

	for _, entry := range entries {
		collection, err := database.GetCollection(entry.Name())
		if err != nil {
			log.Printf("Error loading collection: %v\n", err)
			return
		}
		flushOrphanedMemSegments(collection)
	}
}

func flushOrphanedMemSegments(collection *Collection) {
	for _, memSegment := range collection.orphanedMemSegments {
		sendJob(FlushMemSegment{memSegment})
	}
}
