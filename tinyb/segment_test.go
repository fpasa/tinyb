package tinyb

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func segment(path string) *Segment {
	segment, err := SegmentFromFile(path, 8_192)
	handleError(err)
	return segment
}

func TestSegment_SegmentFromFile(t *testing.T) {
	seg := segment(SegmentFilePath)

	// segmentPath should se correctly set
	assertEqual(seg.segmentPath, SegmentFilePath)

	// The index should contain an entry for the first
	// key and one for the last. Since the file is tiny
	// there should be no other keys in the index.
	assertEqual(len(seg.index), 2)
	assertEqual(seg.index[0].offset, uint64(0))
	assertEqual(seg.index[1].offset, uint64(97))
}

func TestSegment_LastPair(t *testing.T) {
	seg := segment(SegmentFilePath)
	assertEqual(seg.LastPair().offset, uint64(97))
}

func TestSegment_Get(t *testing.T) {
	seg := segment(SegmentLargeFilePath)

	// The smallest key in the test file is 10
	assertEqual(seg.index[0].key, uint64(10))
	// The largest key in the test file is 51_452
	assertEqual(seg.LastPair().key, uint64(51_452))

	// First test that keys outside the range
	// are reported as non-existing
	_, exists, err := seg.Get(1)
	handleError(err)
	assertEqual(exists, false)

	_, exists, err = seg.Get(1_000_000)
	handleError(err)
	assertEqual(exists, false)

	// Then try to get both ends of the index
	value, exists, err := seg.Get(seg.index[0].key)
	handleError(err)
	assertEqual(exists, true)
	assertEqual(value, GetObject(10))

	value, exists, err = seg.Get(seg.LastPair().key)
	handleError(err)
	assertEqual(exists, true)
	assertEqual(value, GetObject(51_452))

	// Get a sample of all possible keys
	for i := 10; i <= 51_452; i += rand.Intn(100) {
		value, exists, err := seg.Get(uint64(i))
		handleError(err)
		assertEqual(exists, true)
		assertEqual(value, GetObject(i))
	}
}

func TestSegment_Get_Performance(t *testing.T) {
	seg := segment(SegmentLargeFilePath)

	start := time.Now()
	// See test above for numbers
	// Sampling with 51 executes about 1000 read operations
	for i := 10; i <= 51_452; i += 51 {
		_, exists, err := seg.Get(uint64(i))
		handleError(err)
		assertEqual(exists, true)
	}
	elapsed := time.Since(start)

	fmt.Printf("Query by key: %v\n", elapsed)
}
