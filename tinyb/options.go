package tinyb

type Options struct {
	// Path to the folder containing the database data.
	DataPath string
	// Host the database listens to.
	Host string
	// Port the database listens to.
	Port string
	// Size at which memory segments are flushed to disk.
	//
	// Decreasing this value will reduce memory consumption
	// of the database, in particular if the database has many
	// collections.
	//
	// Increasing this value makes I/O more efficient and improves
	// the speed at which writes with many objects are performed
	// (up to a point).
	FlushSize uint64
	// Size of the indexing blocks.
	//
	// The database keeps in memory the key at the beginning
	// of each block. When searching for a key, it locates which
	// block contains the key, loads it and scans the block.
	//
	// Increasing this value reduces the memory consumption
	// of the database, especially for large collections,
	// but slows queries down as they need to scan more data.
	//
	// Decreasing the block size speeds up queries at the
	// cost of higher memory consumption.
	IndexBlockSize uint64
}

func DefaultOptions() *Options {
	// TODO: default DataPath on each platform
	return &Options{
		Host:           "0.0.0.0",
		Port:           "55443",
		FlushSize:      4_000_000,
		IndexBlockSize: 8_192,
	}
}
