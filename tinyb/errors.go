package tinyb

import (
	"errors"
)

var (
	NotFoundError = errors.New("Object not found")
)
