package tinyb

import (
	"log"
	"path/filepath"
)

type Database struct {
	// Pointer to options for the database
	options *Options

	loadCollectionQueue chan Message

	// Hash table mapping collection names to the collection
	// objects stored in the database.
	//
	// Loading collections can happen lazily, so this map is
	// not guaranteed to be complete. Use GetCollection to
	// get a collection or load it if not loaded yet.
	collections map[string]*Collection
}

func NewDatabase(options *Options) *Database {
	database := &Database{
		options:             options,
		loadCollectionQueue: make(chan Message, 0),
		collections:         make(map[string]*Collection),
	}

	go database.loadCollectionWorker()

	return database
}

// loadCollectionWorker is a function intended to be run as goroutine,
// which loads collections into the database hash table.
//
// The goal of the function is to execute all collection loading
// in a sequential manner to avoid double loading. Since
// collection loading starts its own write goroutine, this
// is very important to ensure that we don't have duplicate
// goroutines writing to the same files.
func (d *Database) loadCollectionWorker() {
loop:
	for {
		genericMessage := <-d.loadCollectionQueue
		switch message := genericMessage.(type) {
		case loadCollectionMessage:
			// Check if collection already exist
			if _, isLoaded := d.collections[message.collectionName]; isLoaded {
				message.errors <- nil
				continue
			}

			collectionPath := filepath.Join(message.options.DataPath, message.collectionName)
			collection, err := loadCollection(collectionPath, message.options)
			if err != nil {
				message.errors <- err
				continue
			}
			d.collections[message.collectionName] = collection

			message.errors <- nil
		case ExitMessage:
			break loop
		default:
			log.Panicf("Hander for job %v not yet implemented\n", message)
		}
	}
}

// Loads collection in goroutine, wait for loading and return any errors
func (d *Database) loadCollection(
	collectionName string,
	options *Options,
) error {
	errors := make(chan error, 1)
	message := loadCollectionMessage{
		collectionName, options, errors,
	}
	d.loadCollectionQueue <- message
	return <-errors
}

// Close the database and properly shutdowns the load collection goroutine
func (d *Database) Close() {
	d.loadCollectionQueue <- ExitMessage{}
}

// GetCollection returns the collection with the given name, loading
// it if not yet loaded into the database and creating it if not existing.
func (d *Database) GetCollection(collectionName string) (*Collection, error) {
	collection, isLoaded := d.collections[collectionName]
	if !isLoaded {
		err := d.loadCollection(collectionName, d.options)
		if err != nil {
			return nil, err
		}
		collection = d.collections[collectionName]
	}
	return collection, nil
}
