package tinyb

// This file defines a number of messages used in different queues

// Message is an interface used in queues to handle
// different message types
type Message interface{}

// ExitMessage is a special message used to quit
// the worker goroutine
type ExitMessage struct{}

// BACKGROUND JOB MESSAGES

type FlushMemSegment struct {
	memSegment *MemSegment
}

// LOAD COLLECTION MESSAGES

type loadCollectionMessage struct {
	collectionName string
	options        *Options
	errors         chan error
}

// COLLECTION WRITE MESSAGES

// writeObjectMessage is a message asking the collection
// write goroutine to write a record to the collection
type writeObjectMessage struct {
	key    uint64
	data   []byte
	errors chan error
}

// deleteObjectMessage is a message asking the collection
// write goroutine to delete a record from the collection
type deleteObjectMessage struct {
	key    uint64
	errors chan error
}
