package tinyb

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func memSegment(path string) *MemSegment {
	seg, err := NewMemSegment(path, 0)
	handleError(err)
	return seg
}

func TestMemSegment_NewMemSegment(t *testing.T) {
	path := t.TempDir()
	seg := memSegment(path)

	assertEqual(seg.collectionPath, path)
	assertEqual(seg.number, uint64(0))
	assertEqual(len(seg.records), 0)
}

func TestMemSegment_MemSegmentFromLog(t *testing.T) {
	seg, err := MemSegmentFromLog(LogFilePath, 0)
	handleError(err)

	assertEqual(seg.collectionPath, filepath.Dir(LogFilePath))
	assertEqual(seg.number, uint64(0))
	assertEqual(len(seg.records), 5)
}

func TestMemSegment_SetGet(t *testing.T) {
	path := t.TempDir()
	seg := memSegment(path)

	err := seg.Set(123, []byte("123"))
	handleError(err)

	read, exists := seg.Get(123)
	assertEqual(exists, true)
	assertEqual(read, []byte("123"))

	logPath := filepath.Join(path, "log-0")
	assertFileExists(logPath)
}

func TestMemSegment_DeleteNotFound(t *testing.T) {
	seg := memSegment(t.TempDir())
	err := seg.Delete(1)
	assertEqual(err, NotFoundError)
}

func TestMemSegment_Delete(t *testing.T) {
	object := []byte("123")

	seg := memSegment(t.TempDir())
	err := seg.Set(1, object)
	handleError(err)
	key, exists := seg.Get(1)
	assertEqual(exists, true)
	assertEqual(key, object)

	err = seg.Delete(1)
	handleError(err)

	_, exists = seg.Get(1)
	assertEqual(exists, false)
}

func TestMemSegment_Size(t *testing.T) {
	seg := memSegment(t.TempDir())

	size, err := seg.Size()
	handleError(err)
	assertEqual(size, uint64(0))

	// 8 (key) + 4 (length) + 3 (value) + 1 (separator) = 16
	err = seg.Set(1, []byte("123"))
	handleError(err)
	// 8 (key) + 4 (length) + 13 (value) + 1 (separator) = 26
	err = seg.Set(3, []byte("1234567890123"))
	handleError(err)

	size, err = seg.Size()
	handleError(err)
	// The sums of the length above must be 42
	assertEqual(size, uint64(42))
}

func TestMemSegment_Flush(t *testing.T) {
	path := t.TempDir()
	segmentPath := filepath.Join(path, "segment-0")

	memSeg := memSegment(path)

	err := memSeg.Set(10, []byte("10"))
	handleError(err)
	err = memSeg.Set(100, []byte("100"))
	handleError(err)
	err = memSeg.Delete(100)
	handleError(err)
	err = memSeg.Set(5, []byte("5"))
	handleError(err)

	returnedSegmentPath, err := memSeg.Flush()
	handleError(err)
	assertEqual(returnedSegmentPath, segmentPath)

	// Check file existence
	logPath := filepath.Join(path, "log-0")
	assertFileDoesNotExists(logPath)

	assertFileExists(segmentPath)

	// Check content
	file, err := os.Open(segmentPath)
	handleError(err)

	key, value, err := ReadRecord(file)
	handleError(err)
	assertEqual(key, uint64(5))
	assertEqual(value, []byte("5"))

	key, value, err = ReadRecord(file)
	handleError(err)
	assertEqual(key, uint64(10))
	assertEqual(value, []byte("10"))

	_, _, err = ReadRecord(file)
	assertEqual(err, io.EOF)
}

// Test and benchmark speed of different operations
// on a realistic scenario with thousands of objects:
//
// - Inserting 100k records.
// - Querying 100k records.
// - Flushing 100k records.
func TestMemSegment_Get_Performance(t *testing.T) {
	seg := memSegment(t.TempDir())

	// Insert
	start := time.Now()
	for i := 0; i < 100_000; i++ {
		err := seg.Set(uint64(i), Object)
		handleError(err)
	}
	elapsed := time.Since(start)

	size, err := seg.Size()
	handleError(err)
	fmt.Printf("Size: %v kB\n", size/1000)
	fmt.Printf("Insert: %v\n", elapsed)

	// Query
	start = time.Now()
	for i := 0; i < 100_000; i++ {
		seg.Get(uint64(i))
	}
	elapsed = time.Since(start)

	fmt.Printf("Query by key: %v\n", elapsed)

	// Flush
	start = time.Now()
	_, err = seg.Flush()
	handleError(err)
	elapsed = time.Since(start)

	fmt.Printf("Flush: %v\n", elapsed)
}
